/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.animal;

/**
 *
 * @author HP
 */
public class TestAnimal {
    public static void main(String[] args){
        Human h1 = new Human("Tong");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is Land animal ? " + (h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is Land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        
        Cat c1 = new Cat("Meaow");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is Land animal ? " + (c1 instanceof LandAnimal));
        
        Animal a2 = c1;
        System.out.println("a2 is Land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is Poultry animal ? " + (a2 instanceof Poultry));
        
        Dog d1 = new Dog("Boxbox");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is Land animal ? " + (d1 instanceof LandAnimal));
        
        Animal a3 = d1;
        System.out.println("a3 is Land animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is Aquatic animal ? " + (a3 instanceof AquaticAnimal));
        
        Crocodile cro1 = new Crocodile("Somsri");
        cro1.crawl();
        cro1.eat();
        cro1.walk();
        cro1.speak();
        cro1.sleep();
        System.out.println("cro1 is animal ? " + (cro1 instanceof Animal));
        System.out.println("cro1 is Reptile animal ? " + (cro1 instanceof Reptile));
        
        Animal a4 = cro1;
        System.out.println("a4 is Land animal ? " + (a4 instanceof LandAnimal));
        System.out.println("a4 is Reptile animal ? " + (a4 instanceof Reptile));
        System.out.println("a4 is Poultry animal ? " + (a4 instanceof Poultry));
        
        Snake s1 = new Snake("Metung");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is Reptile animal ? " + (s1 instanceof Reptile));
        
        Animal a5 = s1;
        System.out.println("a5 is Land animal ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is Reptile animal ? " + (a5 instanceof Reptile));
        System.out.println("a5 is Aquatic animal ? " + (a5 instanceof AquaticAnimal));
        
        Fish f1 = new Fish("Nemo");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is Aquatic animal ? " + (f1 instanceof AquaticAnimal));
        
        Animal a6 = f1;
        System.out.println("a6 is Land animal ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is Aquatic animal ? " + (a6 instanceof AquaticAnimal));
        System.out.println("a6 is Poultry animal ? " + (a6 instanceof Poultry));
        
        Crab cra1 = new Crab("Meme");
        cra1.swim();
        cra1.eat();
        cra1.walk();
        cra1.speak();
        cra1.sleep();
        System.out.println("cra1 is animal ? " + (cra1 instanceof Animal));
        System.out.println("cra1 is Aquatic animal ? " + (cra1 instanceof AquaticAnimal));
        
        Animal a7 = cra1;
        System.out.println("a7 is Reptile animal ? " + (a7 instanceof Reptile));
        System.out.println("a7 is Aquatic animal ? " + (a7 instanceof AquaticAnimal));
        System.out.println("a7 is Poultry animal ? " + (a7 instanceof Poultry));
        
        Bat b1 = new Bat("Dark");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry animal ? " + (b1 instanceof Poultry));
        
        Animal a8 = b1;
        System.out.println("a8 is Poultry animal ? " + (a8 instanceof Poultry));
        System.out.println("a8 is Aquatic animal ? " + (a8 instanceof AquaticAnimal));
        System.out.println("a8 is Land animal ? " + (a8 instanceof LandAnimal));
        
        Bird b2 = new Bird("Marine");
        b2.fly();
        b2.eat();
        b2.walk();
        b2.speak();
        b2.sleep();
        System.out.println("b2 is animal ? " + (b2 instanceof Animal));
        System.out.println("b2 is Poultry animal ? " + (b2 instanceof Poultry));
        
        Animal a9 = b2;
        System.out.println("a9 is Poultry animal ? " + (a9 instanceof Poultry));
        System.out.println("a9 is Reptile animal ? " + (a9 instanceof Reptile));
        System.out.println("a9 is Land animal ? " + (a9 instanceof LandAnimal));
    }
}
